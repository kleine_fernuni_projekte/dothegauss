#!/usr/bin/env python3

matrix = [[0, 1, 2,-1, 2, 1, 1],
		  [0, 0, 0, 0, 1, 0, 1],
		  [0, 2, 4, 1,-2, 5,-1],
		  [0, 3, 6, 2, 6, 8, 8]]


from fractions import Fraction as Frac
from random import randint

class Matrix(object):

	def __init__(self, matrix):
		self.m = []
		for row in matrix:
			self.m.append(list(row))
		self.rows = len(self.m)
		self.columns = len(self.m[0])
		self.r, self.t = 0, 0
		for i in range(self.rows):
			for j in range(self.columns):
				self.m[i][j] = Frac(self.m[i][j])

	def __str__(self):
		textstring = ''
		for row in self.m:
			for col in row:
				textstring += f' {str(col):>9}'
			textstring += '\n'
		return textstring

	def printMatrix(self):
		print(self)

	def printRankAndPivots(self):
		rank = 0
		lastRow = -1

		for j in range(self.columns):
			for i in range(lastRow + 1, self.rows):
				if self.m[i][j] == 1:
					rank += 1
					lastRow = i
					print(f'Pivot position ({i + 1},{j + 1})')
		print(f'Rank is {rank}')

	def swapRows(self, row1, row2):
		self.m[row1], self.m[row2] = self.m[row2], self.m[row1]
		print(f'swapping rows {row1 + 1} and {row2 + 1}:')
		self.printMatrix()

	def multiplyRow(self, row, scalar):
		for j in range(self.columns):
			self.m[row][j] *= scalar
		print(f'multiplying row {row + 1} with {scalar}:')
		self.printMatrix()

	def addToRow(self, row1, row2, scalar):
		for j in range(self.columns):
			self.m[row1][j] += scalar * self.m[row2][j]
		print(f'adding ({scalar} times row {row2 + 1}) to row {row1 + 1}:')
		self.printMatrix()

	def doTheGauss(self):
		def case2():
			if self.m[self.r][self.t] != 1:
				self.multiplyRow(self.r, Frac(1 / self.m[self.r][self.t]))
			for i in range(self.rows):
				if i == self.r:
					continue
				if self.m[i][self.t] != 0:
					self.addToRow(i, self.r, -self.m[i][self.t])
			self.r += 1
			print(f'r = {self.r}')

		print(f'r = {self.r}')
		self.printMatrix()

		for self.t in range(self.columns):
			print(f't = {self.t + 1}')
			if self.r >= self.rows:
				break

			if self.m[self.r][self.t] == 0:
				# case 1 & 3:
				for i in range(self.r + 1, self.rows):
					if self.m[i][self.t] != 0:
						self.swapRows(self.r, i)
						case2()
			else:
				case2()

		self.printRankAndPivots()

	@staticmethod
	def newRandomMatrix(rows=3, columns=4, valuemin=-5, valuemax=7,
						rowsmin=2, rowsmax=5, colsmin=3, colsmax=6):
		matrix = []
		if rows <= 0:
			rows = randint(rowsmin, rowsmax)
		if columns <= 0:
			columns = randint(colsmin, colsmax)
		for i in range(rows):
			newrow = []
			for j in range(columns):
				newrow.append(randint(valuemin, valuemax))
			matrix.append(newrow)
		return Matrix(matrix)

if __name__ == '__main__':
	m1 = Matrix(matrix)
	m1.doTheGauss()
