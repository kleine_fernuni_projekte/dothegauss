# DoTheGauss

This is a python version of the Gauss algorithm of course 01141, p. 110.

## Use case 1 - from the terminal

Edit `dothegauss.py`, change the matrix as you wish and save your changes.

Then execute the file like this:

```
$ python3 dothegauss.py
-- snip --
```

or like this:

```
$ chmod 755 dothegauss.py
$ ./dothegauss.py
-- snip --
```

## Use case 2 - within Python

First change to the directory where `dothegauss.py` is stored!

Import the `Matrix` class from the `dothegauss` python module:

```
$ python3 -q
>>> from dothegauss import Matrix
```

### Predefined matrices
Create a new matrix and make it a `Matrix` object:

```
>>> matrix = ([1, 2, 3, 4],
...           [5, 6, 7, 8],
...           [9, 0, 1, 2])
>>> m1 = Matrix(matrix)
```

Use its `printMatrix()` method if you like:

```
>>> m1.printMatrix()
         1         2         3         4
         5         6         7         8
         9         0         1         2
```

Use its `doTheGauss()` method:

```
>>> m1.doTheGauss()
r = 0
         1         2         3         4
         5         6         7         8
         9         0         1         2
t = 1
adding (-5 times row 1) to row 2:
         1         2         3         4
         0        -4        -8       -12
         9         0         1         2
adding (-9 times row 1) to row 3:
         1         2         3         4
         0        -4        -8       -12
         0       -18       -26       -34
r = 1
t = 2
multiplying row 2 with -1/4:
         1         2         3         4
         0         1         2         3
         0       -18       -26       -34
adding (-2 times row 2) to row 1:
         1         0        -1        -2
         0         1         2         3
         0       -18       -26       -34
adding (18 times row 2) to row 3:
         1         0        -1        -2
         0         1         2         3
         0         0        10        20
r = 2
t = 3
multiplying row 3 with 1/10:
         1         0        -1        -2
         0         1         2         3
         0         0         1         2
adding (1 times row 3) to row 1:
         1         0         0         0
         0         1         2         3
         0         0         1         2
adding (-2 times row 3) to row 2:
         1         0         0         0
         0         1         0        -1
         0         0         1         2
r = 3
t = 4
```

### Random matrices
Use the static method `newRandomMatrix()` to create a new random Matrix object:

```
>>> m2 = Matrix.newRandomMatrix()
```

This will create a random 3x4 matrix.

Print it:

```
>>> print(m2)
         1        -3        -5         5
        -1         0        -3        -1
        -5         1         4        -4
```

Use its `doTheGauss()` method:

```
>>> m2.doTheGauss()
r = 0
         1        -3        -5         5
        -1         0        -3        -1
        -5         1         4        -4
t = 1
adding (1 times row 1) to row 2:
         1        -3        -5         5
         0        -3        -8         4
        -5         1         4        -4
adding (5 times row 1) to row 3:
         1        -3        -5         5
         0        -3        -8         4
         0       -14       -21        21
r = 1
t = 2
multiplying row 2 with -1/3:
         1        -3        -5         5
         0         1       8/3      -4/3
         0       -14       -21        21
adding (3 times row 2) to row 1:
         1         0         3         1
         0         1       8/3      -4/3
         0       -14       -21        21
adding (14 times row 2) to row 3:
         1         0         3         1
         0         1       8/3      -4/3
         0         0      49/3       7/3
r = 2
t = 3
multiplying row 3 with 3/49:
         1         0         3         1
         0         1       8/3      -4/3
         0         0         1       1/7
adding (-3 times row 3) to row 1:
         1         0         0       4/7
         0         1       8/3      -4/3
         0         0         1       1/7
adding (-8/3 times row 3) to row 2:
         1         0         0       4/7
         0         1         0     -12/7
         0         0         1       1/7
r = 3
t = 4
```

#### Other modifications

You can also create a `Matrix` object of other specific dimensions.
For instance this will create a random 4x7 matrix:

```
>>> m3 = Matrix.newRandomMatrix(4, 7)
>>> print(m3)
        -2         5        -1        -2        -1        -3         0
         2        -5         1         6         1         0         0
         7        -4        -5         0         4         7         3
         1        -4         3         5        -2        -1         5
```

You can determine the minimum value and the maximum value of its elements:

```
>>> m4 = Matrix.newRandomMatrix(3, 8, 0, 1)
>>> print(m4)
         1         1         0         1         1         0         1         1
         1         1         0         1         1         0         1         1
         0         0         1         1         1         1         1         1
```

By setting the first and second parameter to `0` you can create matrices of random dimensions:

```
>>> Matrix.newRandomMatrix(0, 0, -10, 10).printMatrix()
         3        10        -1        -1
        -6        -8        -1         2
>>> Matrix.newRandomMatrix(0, 0, -10, 10).printMatrix()
         4         8         0
         6       -10        -9
        -1        -3       -10
        -6         9        -3
```

Finally you can change the boundaries of those random dimension by setting
the 5th (min rows), 6th (max rows), 7th (min columns) and 8th (max columns)
parameter:

```
>>> m = Matrix.newRandomMatrix(0, 0, -10, 10, 4, 8, 2, 4)
```

This example creates random matrices of 4 rows minimum, 8 rows maximum,
2 columns minimum and 4 columns maximum.
